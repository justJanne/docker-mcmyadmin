FROM openjdk:8-jre

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      libgdiplus

ADD http://mcmyadmin.com/Downloads/MCMA2_glibc26_2.zip /tmp/
ADD http://mcmyadmin.com/Downloads/etc.zip /tmp/
RUN unzip -q /tmp/etc.zip -d /usr/local && \
    mkdir -p /opt/mcmyadmin2 && \
    unzip -q /tmp/MCMA2_glibc26_2.zip -d /opt/mcmyadmin2 && \
    rm -rf /tmp/*

RUN adduser --home /minecraft --shell /bin/bash --disabled-password --system minecraft

VOLUME /minecraft
USER minecraft

EXPOSE 8080
EXPOSE 25565

WORKDIR /minecraft
ENTRYPOINT ["/opt/mcmyadmin2/MCMA2_Linux_x86_64"]
